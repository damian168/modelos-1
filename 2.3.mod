# Objetivo: Calcular la cantidad de dulces Candy y Sweet a producir para maximizar el ingreso, con la cantidad de ingredientes disponibles.
# Hipotesis:
#  - No hay perdidas en la produccion.
#  - Se vende todo lo que se produce.
#  - 

# Declaracion de variables
# ------------------------

var Ca >= 0;  # Cantidad de dulce Candy a producir
var Sw >= 0;  # Cantidad de dulce Sweet a producir

# { <In><Dl>: Cntidad de ingrediente <In> a utilizar para producir el dulce <Dl> }:
var AzCa >= 0;
var NuCa >= 0;
var ChCa >= 0;

var AzSw >= 0;
var NuSw >= 0;
var ChSw >= 0;

# Cantidad total de cada ingrediente:
var Az >= 0;
var Nu >= 0;
var Ch >= 0;

# Definicion del funcional: maximizar el ingreso
maximize z: 25 * Ca + 20 * Sw ;

# Restricciones:
# -------------

# Ingredientes:
s.t. az: Az = AzCa + AzSw;
s.t. nu: Nu = NuCa + NuSw;
s.t. ch: Ch = ChCa + ChSw;

# Disponibilidad de ingredientes:
s.t. DispAz: Az <= 100;
s.t. DispNu: Nu <= 20;
s.t. DispCh: Ch <= 30;

# Minimos de ingredientes en la mezcla:
s.t. MinNuCa: NuCa >= 0.2 * Ca;
s.t. MinNuSw: NuSw >= 0.1 * Sw;
s.t. MinChSw: ChSw >= 0.1 * Sw;

# Mezclas:
s.t. Candy: Ca = AzCa + NuCa + ChCa;
s.t. Sweet: Sw = AzSw + NuSw + ChSw;

end;
