# Resolucion 1 - Contenido del archivo 1.mod.

/* Declaracion de variables */
var A >= 0;
var B >= 0;
var C >= 0;

/* Definicion del funcional */
maximize z: A + B + C ;

/* Restricciones */

/* Procesamiento de cada equipo */
s.t. procEq1: A + B - C = 100;
s.t. procEq2: C + A <= 180;
s.t. procEq3: 0.6 * A + 1 * B + 0.6 * C <= 110;

/* Demandas maximas y minimas */
s.t. demMaxA: A <= 100;
s.t. demMaxB: B <= 120;
s.t. demMinB: B >= 80;
end;