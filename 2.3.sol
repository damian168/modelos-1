Problem:    2
Rows:       12
Columns:    11
Non-zeros:  28
Status:     OPTIMAL
Objective:  z = 0 (MINimum)

   No.   Row name   St   Activity     Lower bound   Upper bound    Marginal
------ ------------ -- ------------- ------------- ------------- -------------
     1 z            B              0                             
     2 az           NS             0            -0             =         < eps
     3 nu           NS             0            -0             =         < eps
     4 ch           NS             0            -0             =         < eps
     5 DispAz       B              0                         100 
     6 DispNu       B              0                          20 
     7 DispCh       B              0                          30 
     8 MinNuCa      B              0            -0               
     9 MinNuSw      B              0            -0               
    10 MinChSw      B              0            -0               
    11 Candy        NS             0            -0             =         < eps
    12 Sweet        NS             0            -0             =         < eps

   No. Column name  St   Activity     Lower bound   Upper bound    Marginal
------ ------------ -- ------------- ------------- ------------- -------------
     1 Ca           NL             0             0                          25 
     2 Sw           NL             0             0                          20 
     3 AzCa         NL             0             0                       < eps
     4 NuCa         NL             0             0                       < eps
     5 ChCa         B              0             0               
     6 AzSw         B              0             0               
     7 NuSw         NL             0             0                       < eps
     8 ChSw         NL             0             0                       < eps
     9 Az           B              0             0               
    10 Nu           B              0             0               
    11 Ch           B              0             0               

Karush-Kuhn-Tucker optimality conditions:

KKT.PE: max.abs.err = 0.00e+00 on row 0
        max.rel.err = 0.00e+00 on row 0
        High quality

KKT.PB: max.abs.err = 0.00e+00 on row 0
        max.rel.err = 0.00e+00 on row 0
        High quality

KKT.DE: max.abs.err = 0.00e+00 on column 0
        max.rel.err = 0.00e+00 on column 0
        High quality

KKT.DB: max.abs.err = 0.00e+00 on row 0
        max.rel.err = 0.00e+00 on row 0
        High quality

End of output
