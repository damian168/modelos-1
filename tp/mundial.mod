# Conjuntos
set EQUIPOS;
set PARTIDOS;
set PARTIDOS_EN_RIO;
set PARTIDOS_NO_EN_RIO;
set PARTIDOS_OBLIGATORIOS;
set DIAS;
set CIUDADES;
set ITINERARIOS;

# Parametros
param PARTIDO_CIUDAD{p in PARTIDOS} >= 0;
param PARTIDO_DIA{p in PARTIDOS} >= 0;

param INGRESO{p in PARTIDOS} >= 0;
param COSTO{i in ITINERARIOS} >= 0;
param PARADAS_IT{i in ITINERARIOS} >= 0;
param M >= 0;
param CANT_EQUIPOS >= 0;

#Definicion de variables
#=======================
# Presencia de un equipo 'e' en la ciudad 'c' en un dia 'd'.
var E{e in EQUIPOS, c in CIUDADES, d in DIAS} >= 0, binary;

# Presencia de un equipo 'e' en la ciudad 'c' en un dia 'd', llegando a partir de haber tomado el itinerario 'i'
var EiT{i in ITINERARIOS, e in EQUIPOS, c in CIUDADES, d in DIAS} >= 0, binary;

# Si el partido 'p' llego a cubrirse.
var P{p in PARTIDOS} >= 0, binary;

# Si el equipo 'e' tomo el tour 'i'
var I{e in EQUIPOS, i in ITINERARIOS} >= 0, binary;

var COSTOS >= 0;
var INGRESOS >= 0;

/* Funcional */
maximize z: INGRESOS - COSTOS;

# Los ingresos solo provienen de la transmision de los partidos
s.t. ingresos: INGRESOS = sum{p in PARTIDOS} INGRESO[p] * P[p];

# Los costos son lo de los itinerarios tomados
s.t. costos: COSTOS = sum{i in ITINERARIOS, e in EQUIPOS} COSTO[i] * I[e,i];

/* Restricciones */

# Partidos obligatorios
s.t. pOblig{p in PARTIDOS_OBLIGATORIOS}: P[p] = 1;

# Partidos "gratis" en RIO
s.t. pEnRio{p in PARTIDOS_EN_RIO}: P[p] = 1;

# Si un equipo esta en la misma ciudad/dia que un partido, lo cubre.
# Armo un OR con la presencia de cada equipo en la ciudad/dia del partido:
s.t. transP1{p in PARTIDOS_NO_EN_RIO}: sum{e in EQUIPOS} E[e,PARTIDO_CIUDAD[p],PARTIDO_DIA[p]] <= P[p] * CANT_EQUIPOS;
s.t. transP2{p in PARTIDOS_NO_EN_RIO}: sum{e in EQUIPOS} E[e,PARTIDO_CIUDAD[p],PARTIDO_DIA[p]] >= P[p];

# Un equipo no puede estar en mas de una ciudad en un dia determinado
s.t. e1Ciud{e in EQUIPOS, d in DIAS}: sum{c in CIUDADES} E[e,c,d] <= 1;

# ITINERARIOS:
# Un itinerario fue tomado si un equipo estuvo en todas sus combinaciones ciudad/dia
s.t. it1{e in EQUIPOS}: EiT[1,e,1,2] + EiT[1,e,8,3] + EiT[1,e,5,4] + EiT[1,e,4,5] + EiT[1,e,10,6] = PARADAS_IT[1] * I[e,1];
s.t. it2{e in EQUIPOS}: EiT[2,e,2,3] + EiT[2,e,11,4] + EiT[2,e,7,5] + EiT[2,e,8,6] + EiT[2,e,10,7] = PARADAS_IT[2] * I[e,2];
s.t. it3{e in EQUIPOS}: EiT[3,e,7,4] + EiT[3,e,4,5] + EiT[3,e,8,6] + EiT[3,e,11,7] + EiT[3,e,10,8] = PARADAS_IT[3] * I[e,3];
s.t. it4{e in EQUIPOS}: EiT[4,e,4,5] + EiT[4,e,8,6] + EiT[4,e,2,7] + EiT[4,e,12,8] + EiT[4,e,10,9] = PARADAS_IT[4] * I[e,4];
s.t. it5{e in EQUIPOS}: EiT[5,e,9,1] + EiT[5,e,1,2] + EiT[5,e,10,3] = PARADAS_IT[5] * I[e,5];
s.t. it6{e in EQUIPOS}: EiT[6,e,2,7] + EiT[6,e,3,8] + EiT[6,e,6,9] + EiT[6,e,10,10] = PARADAS_IT[6] * I[e,6];
s.t. it7{e in EQUIPOS}: EiT[7,e,4,1] + EiT[7,e,1,2] + EiT[7,e,6,3] + EiT[7,e,2,4] + EiT[7,e,7,5] + EiT[7,e,10,6] = PARADAS_IT[7] * I[e,7];
s.t. it8{e in EQUIPOS}: EiT[8,e,1,2] + EiT[8,e,8,3] + EiT[8,e,11,4] + EiT[8,e,7,5] + EiT[8,e,10,6] = PARADAS_IT[8] * I[e,8];
s.t. it9{e in EQUIPOS}: EiT[9,e,11,4] + EiT[9,e,7,5] + EiT[9,e,8,6] + EiT[9,e,2,7] + EiT[9,e,10,8] = PARADAS_IT[9] * I[e,9];
s.t. it10{e in EQUIPOS}: EiT[10,e,7,5] + EiT[10,e,8,6] + EiT[10,e,10,7] = PARADAS_IT[10] * I[e,10];
s.t. it11{e in EQUIPOS}: EiT[11,e,11,7] + EiT[11,e,10,8] = PARADAS_IT[11] * I[e,11];
s.t. it12{e in EQUIPOS}: EiT[12,e,2,7] + EiT[12,e,4,8] + EiT[12,e,5,9] + EiT[12,e,10,10] = PARADAS_IT[12] * I[e,12];
s.t. it13{e in EQUIPOS}: EiT[13,e,4,1] + EiT[13,e,5,2] + EiT[13,e,2,3] + EiT[13,e,12,4] + EiT[13,e,10,5] = PARADAS_IT[13] * I[e,13];
s.t. it14{e in EQUIPOS}: EiT[14,e,1,9] + EiT[14,e,8,10] + EiT[14,e,5,11] + EiT[14,e,4,12] + EiT[14,e,10,13] = PARADAS_IT[14] * I[e,14];
s.t. it15{e in EQUIPOS}: EiT[15,e,2,10] + EiT[15,e,11,11] + EiT[15,e,7,12] + EiT[15,e,8,13] + EiT[15,e,10,14] = PARADAS_IT[15] * I[e,15];
s.t. it16{e in EQUIPOS}: EiT[16,e,7,11] + EiT[16,e,4,12] + EiT[16,e,8,13] + EiT[16,e,11,14] + EiT[16,e,10,15] = PARADAS_IT[16] * I[e,16];
s.t. it17{e in EQUIPOS}: EiT[17,e,4,12] + EiT[17,e,8,13] + EiT[17,e,2,14] + EiT[17,e,12,15] + EiT[17,e,10,16] = PARADAS_IT[17] * I[e,17];
s.t. it18{e in EQUIPOS}: EiT[18,e,9,8] + EiT[18,e,1,9] + EiT[18,e,10,10] = PARADAS_IT[18] * I[e,18];
s.t. it19{e in EQUIPOS}: EiT[19,e,2,14] + EiT[19,e,3,15] + EiT[19,e,6,16] + EiT[19,e,10,17] = PARADAS_IT[19] * I[e,19];
s.t. it20{e in EQUIPOS}: EiT[20,e,4,8] + EiT[20,e,1,9] + EiT[20,e,6,10] + EiT[20,e,2,11] + EiT[20,e,7,12] + EiT[20,e,10,13] = PARADAS_IT[20] * I[e,20];
s.t. it21{e in EQUIPOS}: EiT[21,e,1,9] + EiT[21,e,8,10] + EiT[21,e,11,11] + EiT[21,e,7,12] + EiT[21,e,10,13] = PARADAS_IT[21] * I[e,21];
s.t. it22{e in EQUIPOS}: EiT[22,e,11,11] + EiT[22,e,7,12] + EiT[22,e,8,13] + EiT[22,e,2,14] + EiT[22,e,10,15] = PARADAS_IT[22] * I[e,22];
s.t. it23{e in EQUIPOS}: EiT[23,e,7,12] + EiT[23,e,8,13] + EiT[23,e,10,14] = PARADAS_IT[23] * I[e,23];
s.t. it24{e in EQUIPOS}: EiT[24,e,11,14] + EiT[24,e,10,15] = PARADAS_IT[24] * I[e,24];
s.t. it25{e in EQUIPOS}: EiT[25,e,2,14] + EiT[25,e,4,15] + EiT[25,e,5,16] + EiT[25,e,10,17] = PARADAS_IT[25] * I[e,25];
s.t. it26{e in EQUIPOS}: EiT[26,e,4,8] + EiT[26,e,5,9] + EiT[26,e,2,10] + EiT[26,e,12,11] + EiT[26,e,10,12] = PARADAS_IT[26] * I[e,26];

# Si se toma un itinerario, solo se ocupa la cantidad de dias de su longitud.
s.t. itAll{i in ITINERARIOS, e in EQUIPOS}: sum{d in DIAS, c in CIUDADES} EiT[i, e, c, d] <= PARADAS_IT[i] * I[e,i];

# Si algun itinerario llevo a un equipo a una ciudad/dia, ese equipo esta ese dia en esa cuidad/dia
s.t. E_EiT_1{e in EQUIPOS, c in CIUDADES, d in DIAS}: sum{i in ITINERARIOS} EiT[i, e, c, d] <= 26 * E[e, c, d];
s.t. E_EiT_2{e in EQUIPOS, c in CIUDADES, d in DIAS}: sum{i in ITINERARIOS} EiT[i, e, c, d] >= E[e, c, d];

solve;
end;