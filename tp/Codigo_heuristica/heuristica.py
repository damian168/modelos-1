#!/usr/bin/env python
import json

class Equipo:

	def __init__(self, nombre, campeon, obligatorio):
		self.campeon = campeon
		self.nombre = nombre
		self.obligatorio = obligatorio

	def __repr__(self):
		return "Equipo(%s, %s, %s)" % (self.nombre, self.campeon, self.obligatorio)

	@staticmethod
	def load():
		equipos = json.load(file('equipos.json'))
		return [Equipo(e['nombre'], e['campeon'], e['obligatorio']) for e in equipos]


class Partido:

	def __init__(self, e1, e2, c, d):
		self.equipo1 = e1
		self.equipo2 = e2
		self.ciudad = c
		self.dia = d

	def __repr__(self):
		return "d:%sc:%s|%s-%s" % (self.dia, self.ciudad, self.equipo1.nombre, self.equipo2.nombre)

	def ingreso(self):
		if self.equipo1.campeon or self.equipo2.campeon:
			return 20000
		else:
			return 10000

	def score(self):
		if self.ciudad == 'RJ':
			return 0
		if self.equipo1.obligatorio or self.equipo2.obligatorio:
			return 20000 * 14
		return self.ingreso()

	def obligatorio(self):
		return self.equipo1.obligatorio or self.equipo2.obligatorio

	def __hash__(self):
		return hash(self.ciudad) ^ hash(self.dia)

	def __eq__(self, other):
		return self.ciudad == other.ciudad and self.dia == other.dia

	@staticmethod
	def load():
		partidos = json.load(file('partidos.json'))
		equipos = { e.nombre: e for e in Equipo.load() }
		return [Partido(equipos[p['equipo1']], equipos[p['equipo2']], p['ciudad'], p['dia']) for p in partidos]


class Estadia:

	def __init__(self, ciudad, dia):
		self.ciudad = ciudad
		self.dia = dia

	def __repr__(self):
		return "[ciudad:%s dia:%d]" % (self.ciudad, self.dia)

	def __hash__(self):
		return hash(self.ciudad) ^ hash(self.dia)

	def __eq__(self, other):
		return self.ciudad == other.ciudad and self.dia == other.dia


class Itinerario:

	def __init__(self, id, estadias, costo):
		self.tomado = False
		self.id = id + 1
		self.estadias = estadias
		self.costo = costo

	def __repr__(self):
		return "(id:%i estadias:%s costo:%i, partidos:%s)" % (self.id, self.estadias, self.costo, self.partidos_cubiertos())

	def dias_set(self):
		return {e.dia for e in self.estadias}

	def incluye_partido(self, partido):
		for e in self.estadias:
			if (e.dia == partido.dia) and (e.ciudad == partido.ciudad):
				return True
		return False

	def partidos_cubiertos(self):
		partidos = Partido.load()
		return [p for p in partidos if self.incluye_partido(p)]

	def score(self):
		return (sum([p.score() for p in self.partidos_cubiertos()]) - self.costo) / len(self.estadias)

	@staticmethod
	def load():
		itinerarios = json.load(file('itinerarios.json'))
		return [Itinerario(i['id'], [Estadia(e['ciudad'], e['dia']) for e in i['estadias']], i['costo']) for i in itinerarios]


class EquipoPeriodistico:

	def __init__(self, id):
		self.calendario = [Estadia('RJ', i) for i in range(1, 18)]
		self.dias_ocupados = set()
		self.itinerarios = []
		self.id = id

	def __repr__(self):
		return "id:%i, calendario:%s, dias_ocupados:%s, itinerarios:%s" % (self.id, self.calendario, self.dias_ocupados, self.itinerarios)

	def puede_tomar_itinerario(self, itinerario):
		return self.dias_ocupados.isdisjoint(itinerario.dias_set())

	def tomar_itinerario(self, itinerario):
		itinerario.tomado = True
		self.dias_ocupados.update(itinerario.dias_set())
		self.itinerarios.append(itinerario)
		for e in itinerario.estadias:
			self.calendario[e.dia - 1] = e

	def partidos_transmitidos(self):
		partidos_transmitidos = set()
		for i in self.itinerarios:
			for p in i.partidos_cubiertos():
				partidos_transmitidos.add(p)
		return partidos_transmitidos

	def costo_itinerarios(self):
		return sum([i.costo for i in self.itinerarios])


def cpm_itinerarios(i1, i2):
	return i1.score() - i2.score()

def z(equiposPeriodisticos):
	partidos_transmitidos = set()
	for ep in equiposPeriodisticos:
		partidos_transmitidos.update(ep.partidos_transmitidos())

	partidos_transmitidos.update(partidos_rj())

	ingreso_total = sum([p.ingreso() for p in partidos_transmitidos])
	print "ingreso: %d" % ingreso_total
	costo_total = sum([ep.costo_itinerarios() for ep in equiposPeriodisticos])
	print "costo: %d" % costo_total

	return ingreso_total - costo_total

def partidos_rj():
	partidos = Partido.load()
	return set([p for p in partidos if p.ciudad == 'RJ'])

def main():
	itinerarios = Itinerario.load()
	equiposPeriodisticos = [EquipoPeriodistico(i) for i in range(4)]
	itinerarios_ordenados = sorted(itinerarios, cpm_itinerarios, reverse=True)

	for i in itinerarios_ordenados:
		for ep in equiposPeriodisticos:
			if ep.puede_tomar_itinerario(i) and not i.tomado:
				ep.tomar_itinerario(i)

	print "beneficio: %s" % z(equiposPeriodisticos)

if __name__ == '__main__':
	main()
