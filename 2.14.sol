Problem:    2
Rows:       53
Columns:    42
Non-zeros:  105
Status:     OPTIMAL
Objective:  z = 49414.58333 (MAXimum)

   No.   Row name   St   Activity     Lower bound   Upper bound    Marginal
------ ------------ -- ------------- ------------- ------------- -------------
     1 entrAC1      NS             0            -0             =           -15 
     2 rendC1       NS             0            -0             =       21.4286 
     3 entrC1_C2    NS             0            -0             =      -55.9375 
     4 entrB_C2     NS             0            -0             =           -20 
     5 rendC2       NS             0            -0             =         48.75 
     6 entrB_C3     NS             0            -0             =           -20 
     7 entrC_C3     NS             0            -0             =           -25 
     8 rendC3       NS             0            -0             =       25.5556 
     9 entrC2_C4    NS             0            -0             =        -48.75 
    10 entrC5_C4    NS             0            -0             =          -120 
    11 rendC4       NS             0            -0             =            90 
    12 entrC2_C5    NS             0            -0             =        -48.75 
    13 entrC3_C5    NS             0            -0             =      -25.5556 
    14 rendC5       NS             0            -0             =       43.5417 
    15 salidaC1     NS             0            -0             =      -55.9375 
    16 salidaC2     NS             0            -0             =        -48.75 
    17 salidaC3     NS             0            -0             =      -25.5556 
    18 salidaC4     NS             0            -0             =           -90 
    19 salidaC5     NS             0            -0             =          -120 
    20 distribucionA
                    NS             0            -0             =           -15 
    21 distribucionB
                    NS             0            -0             =           -20 
    22 distribucionC
                    NS             0            -0             =           -25 
    23 horasC1      NS             0            -0             =       34.5089 
    24 horasC2      NS             0            -0             =         < eps
    25 horasC3      NS             0            -0             =         < eps
    26 horasC4      NS             0            -0             =         < eps
    27 horasC5      NS             0            -0             =       76.4583 
    28 maxHorasC1   NU            70                          70       345.089 
    29 maxHorasC2   B          43.75                          50 
    30 maxHorasC3   B             30                          35 
    31 maxHorasC4   B        10.9375                          60 
    32 maxHorasC5   NU            50                          50       1529.17 
    33 minHorasC4   B        10.9375            10               
    34 minHorasC5   B             50            10               
    35 maxDisponibilidadA
                    B           1000                        2000 
    36 maxDisponibilidadB
                    B        508.333                        3000 
    37 maxDisponibilidadC
                    B            500                        3000 
    38 horasHombreC1
                    NS             0            -0             =         < eps
    39 horasHombreC4
                    NS             0            -0             =         < eps
    40 horasHombreC5
                    NS             0            -0             =         < eps
    41 horasHombreC23enC2
                    NL             0            -0                       < eps
    42 horasHombreC23enC3
                    B          13.75            -0               
    43 totalHorasHombre
                    NS             0            -0             =         < eps
    44 maxHorasHombre
                    B        235.625                         400 
    45 vaporUsado   NS             0            -0             =         < eps
    46 combinacionVapores
                    NS             0            -0             =         < eps
    47 maxVaporNormal
                    B          50000                       50000 
    48 costoVapor   NS         50000         50000             =            -1 
    49 costoOperadores
                    NS          1200          1200             =            -1 
    50 costoMateriaPrima
                    NS             0            -0             =            -1 
    51 costoTotal   NS             0            -0             =            -1 
    52 ingresos     NS             0            -0             =             1 
    53 z            B        49414.6                             

   No. Column name  St   Activity     Lower bound   Upper bound    Marginal
------ ------------ -- ------------- ------------- ------------- -------------
     1 A            B           1000             0               
     2 B            B        508.333             0               
     3 C            B            500             0               
     4 A_C1         B           1000             0               
     5 B_C2         B            175             0               
     6 B_C3         B        333.333             0               
     7 C_C3         B            500             0               
     8 C1_C2        B            700             0               
     9 C2_C4        B            375             0               
    10 C2_C5        B            500             0               
    11 C3_C5        B            750             0               
    12 C5_C4        B          93.75             0               
    13 EntrC1       B           1000             0               
    14 EntrC2       B            875             0               
    15 EntrC3       B        833.333             0               
    16 EntrC4       B         468.75             0               
    17 EntrC5       B           1250             0               
    18 SalC1        B            700             0               
    19 SalC2        B            875             0               
    20 SalC3        B            750             0               
    21 SalC4        B        328.125             0               
    22 SalC5        B           1000             0               
    23 HC1          B             70             0               
    24 HC2          B          43.75             0               
    25 HC3          B             30             0               
    26 HC4          B        10.9375             0               
    27 HC5          B             50             0               
    28 HHC1         B             70             0               
    29 HHC23        B          43.75             0               
    30 HHC4         B         21.875             0               
    31 HHC5         B            100             0               
    32 HH           B        235.625             0               
    33 P1           B        328.125             0               
    34 P2           B         906.25             0               
    35 VapN         B          50000             0               
    36 VapEx        NL             0             0                          -2 
    37 Vap          B          50000             0               
    38 CostoMatPrima
                    B        37666.7             0               
    39 CostoOp      B           1200             0               
    40 CostoVap     B          50000             0               
    41 Costo        B        88866.7             0               
    42 Ingr         B         138281             0               

Karush-Kuhn-Tucker optimality conditions:

KKT.PE: max.abs.err = 7.28e-12 on row 45
        max.rel.err = 1.51e-16 on row 25
        High quality

KKT.PB: max.abs.err = 0.00e+00 on row 0
        max.rel.err = 0.00e+00 on row 0
        High quality

KKT.DE: max.abs.err = 2.84e-14 on column 22
        max.rel.err = 2.16e-16 on column 14
        High quality

KKT.DB: max.abs.err = 0.00e+00 on row 0
        max.rel.err = 0.00e+00 on row 0
        High quality

End of output
