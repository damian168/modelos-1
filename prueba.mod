var y1 >= 0, binary;
var y2 >= 0, binary;
var y3 >= 0, binary;
var y3_a >= 0, binary;
var y3_b >= 0, binary;
var y4 >= 0, binary;
var y5 >= 0, binary;
var ya >= 0, binary;
var yb >= 0, binary;

s.t. a: y1 + y2 + y3_a <= 3 - 1 + ya;
s.t. a_: 3 * ya = y1 + y2 + y3_a;
s.t. b: y3_b + y4 <= 2 -1 + yb;
s.t. b_: 2 * yb <= y3_b + y4;

s.t. y3_or1: y3_a + y3_b <= 2 * y3;
s.t. y3_or2: y3 <= y3_a + y3_b;

s.t. y5_r:y5 = 0;

maximize z: (y1 + y2 + y3 + y4 + y5) - ya - yb;