# Objetivo: Calcular la cantidad de P1 y P2 a producir para obtener el maximo beneficio semanal.
# Hipotesis:
# - No hay inflacion.
# - No hay stock inicial.
# - No hay perdidas de materiales en el trasporte entre centros.
# - No se tomoran en cuenta costos extras a los mencionados.
# - Se vende todo lo que se produce.
# - Los operarios pueden hacer cualquier tipo de tareas.
# - La velocidad de produccion se calculó en funcion de la salida de cada centro.
# - Los rendimientos estan calculados como salida/entrada.
# - De ser necesario puede haber un empleado atendiendo SOLO el centro 2 ó 3 sin necesidad de atender los dos en simultaneo.

# - La disponibilidad de los centros se calculo teniendo en cuenta los distintos 

# Declaracion de variables
# ------------------------

# Cantidad de materia prima a usar
var A >= 0;
var B >= 0;
var C >= 0;

# X_Y: Cantidad de X materia prima que entra al centro Y
var A_C1 >= 0;
var B_C2 >= 0;
var B_C3 >= 0;
var C_C3 >= 0;

# X_Y: Cantidad de salida del centro X que entra al centro Y
var C1_C2 >= 0;
var C2_C4 >= 0;
var C2_C5 >= 0;
var C3_C5 >= 0;
var C5_C4 >= 0;

# Entrada total de cada centro
var EntrC1 >= 0;
var EntrC2 >= 0;
var EntrC3 >= 0;
var EntrC4 >= 0;
var EntrC5 >= 0;

# Salida total de cada centro
var SalC1 >= 0;
var SalC2 >= 0;
var SalC3 >= 0;
var SalC4 >= 0;
var SalC5 >= 0;

# Horas activas de cada centro
var HC1 >= 0;
var HC2 >= 0;
var HC3 >= 0;
var HC4 >= 0;
var HC5 >= 0;

# Horas hombre consumidas por cada centro
var HHC1 >= 0;
var HHC23 >= 0;
var HHC4 >= 0;
var HHC5 >= 0;
# Horas hombre total utilizadas
var HH >= 0;

# Cantidad de cada producto producido
var P1 >= 0;
var P2 >= 0;

# Cantidad de vapor normal consumido, cantidad de vapor adicional, y cantidad total.
var VapN >= 0; 
var VapEx >= 0;
var Vap >= 0;

# Costos discriminados:
var CostoMatPrima >= 0;
var CostoOp >= 0;
var CostoVap >= 0;

# Costos e ingresos totales
var Costo >= 0;
var Ingr >= 0;


# Restricciones:
# -------------

# Centro 1:
s.t. entrAC1: A_C1 = EntrC1;
s.t. rendC1: SalC1 = 0.7 * EntrC1;

# Centro 2:
s.t. entrC1_C2: C1_C2 = 0.8 * EntrC2;
s.t. entrB_C2: B_C2 = 0.2 * EntrC2;
s.t. rendC2: SalC2 = EntrC2;

# Centro 3:
s.t. entrB_C3: B_C3 = 0.4 * EntrC3;
s.t. entrC_C3: C_C3 = 0.6 * EntrC3;
s.t. rendC3: SalC3 = 0.9 * EntrC3;

# Centro 4:
s.t. entrC2_C4: C2_C4 = 0.8 * EntrC4;
s.t. entrC5_C4: C5_C4 = 0.2 * EntrC4;
s.t. rendC4: SalC4 = 0.7 * EntrC4;

# Centro 5:
s.t. entrC2_C5: C2_C5 = 0.4 * EntrC5;
s.t. entrC3_C5: C3_C5 = 0.6 * EntrC5;
s.t. rendC5: SalC5 = 0.8 * EntrC5;

# Conecciones:
s.t. salidaC1: SalC1 = C1_C2;
s.t. salidaC2: SalC2 = C2_C4 + C2_C5;
s.t. salidaC3: SalC3 = C3_C5;
s.t. salidaC4: SalC4 = P1;
s.t. salidaC5: SalC5 = C5_C4 + P2;
s.t. distribucionA: A = A_C1;
s.t. distribucionB: B = B_C2 + B_C3;
s.t. distribucionC: C = C_C3;

# Produccion por semana:
s.t. horasC1: SalC1 = 10 * HC1;
s.t. horasC2: SalC2 = 20 * HC2;
s.t. horasC3: SalC3 = 25 * HC3;
s.t. horasC4: SalC4 = 30 * HC4;
s.t. horasC5: SalC5 = 20 * HC5;

# Disponibilidad de centros:
s.t. maxHorasC1: HC1 <= 70;
s.t. maxHorasC2: HC2 <= 50;
s.t. maxHorasC3: HC3 <= 35;
s.t. maxHorasC4: HC4 <= 60;
s.t. maxHorasC5: HC5 <= 50;

# Horas de trabajo minimas para C4 y C5:
s.t. minHorasC4: HC4 >= 10;
s.t. minHorasC5: HC5 >= 10;

# Disponibilidad materia prima:
s.t. maxDisponibilidadA: A <= 2000;
s.t. maxDisponibilidadB: B <= 3000;
s.t. maxDisponibilidadC: C <= 3000;

# Horas hombre:

s.t. horasHombreC1: HHC1 = HC1;
s.t. horasHombreC4: HHC4 = 2 * HC4;
s.t. horasHombreC5: HHC5 = 2 * HC5;

# Simultaneidad de operario entre C2 y C3:
# Al querer minimizar el valor de HH, esto va a forzar que HHC23 sea tan grande como el mayor entre {HC2, HC3}
# Verificarlo en la solucion
s.t. horasHombreC23enC2: HHC23 >= HC2;
s.t. horasHombreC23enC3: HHC23 >= HC3;

s.t. totalHorasHombre: HH = HHC1 + HHC4 + HHC5 + HHC23;
s.t. maxHorasHombre: HH <= 8 * 5 * 10;  # N horas hombre

# Vapor:

s.t. vaporUsado: Vap = 1000 * HC5;
s.t. combinacionVapores: Vap = VapN + VapEx;
s.t. maxVaporNormal: VapN <= 50000;

# Funcional: Ingresos - Costos

s.t. costoVapor: CostoVap = 1 * VapN + 1 * (50000 - VapN) + 2 * VapEx;
s.t. costoOperadores: CostoOp = 3 * (8 * 5 * 10);  # N horas hombre
s.t. costoMateriaPrima: CostoMatPrima = 15 * A + 20 * B + 25 * C;
s.t. costoTotal: Costo = CostoVap + CostoOp + CostoMatPrima;

s.t. ingresos: Ingr = 90 * P1 + 120 * P2;

maximize z: Ingr - Costo;