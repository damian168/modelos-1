Problem:    2
Rows:       9
Columns:    8
Non-zeros:  20
Status:     OPTIMAL
Objective:  z = 571.6666667 (MINimum)

   No.   Row name   St   Activity     Lower bound   Upper bound    Marginal
------ ------------ -- ------------- ------------- ------------- -------------
     1 z            B        571.667                             
     2 cantSn       NS         10000         10000             =     0.0583333 
     3 cantSnEnML   NS             0            -0             =    -0.0583333 
     4 cantSnEnMR   NS             0            -0             =    -0.0583333 
     5 cantSc       NS          9000          9000             =     0.0416667 
     6 cantScEnML   NS             0            -0             =    -0.0416667 
     7 cantScEnMR   NS             0            -0             =    -0.0416667 
     8 dispML       B        281.667                         300 
     9 dispMR       NU           290                         290      -1.33333 

   No. Column name  St   Activity     Lower bound   Upper bound    Marginal
------ ------------ -- ------------- ------------- ------------- -------------
     1 SnL          B              0             0               
     2 SnR          B          10000             0               
     3 ScL          B           6760             0               
     4 ScR          B           2240             0               
     5 MLSc         B        281.667             0               
     6 MRSc         B             40             0               
     7 MLSn         NL             0             0                   0.0666667 
     8 MRSn         B            250             0               

Karush-Kuhn-Tucker optimality conditions:

KKT.PE: max.abs.err = 1.82e-12 on row 5
        max.rel.err = 1.01e-16 on row 7
        High quality

KKT.PB: max.abs.err = 0.00e+00 on row 0
        max.rel.err = 0.00e+00 on row 0
        High quality

KKT.DE: max.abs.err = 4.44e-16 on column 6
        max.rel.err = 7.84e-17 on column 6
        High quality

KKT.DB: max.abs.err = 0.00e+00 on row 0
        max.rel.err = 0.00e+00 on row 0
        High quality

End of output
