Problem:    hola
Rows:       7
Columns:    3
Non-zeros:  14
Status:     OPTIMAL
Objective:  z = 130 (MAXimum)

   No.   Row name   St   Activity     Lower bound   Upper bound    Marginal
------ ------------ -- ------------- ------------- ------------- -------------
     1 z            B            130                             
     2 procEq1      NS           100           100             =         < eps
     3 procEq2      B             50                         180 
     4 procEq3      NU           110                         110       1.66667 
     5 demMaxA      B             35                         100 
     6 demMaxB      B             80                         120 
     7 demMinB      NL            80            80                   -0.666667 

   No. Column name  St   Activity     Lower bound   Upper bound    Marginal
------ ------------ -- ------------- ------------- ------------- -------------
     1 A            B             35             0               
     2 B            B             80             0               
     3 C            B             15             0               

Karush-Kuhn-Tucker optimality conditions:

KKT.PE: max.abs.err = 0.00e+00 on row 0
        max.rel.err = 0.00e+00 on row 0
        High quality

KKT.PB: max.abs.err = 0.00e+00 on row 0
        max.rel.err = 0.00e+00 on row 0
        High quality

KKT.DE: max.abs.err = 0.00e+00 on column 0
        max.rel.err = 0.00e+00 on column 0
        High quality

KKT.DB: max.abs.err = 0.00e+00 on row 0
        max.rel.err = 0.00e+00 on row 0
        High quality

End of output
