# Objetivo: Como distribuir los estampados en las maquina lentas y rapidas de manera de cumplir con el pedido en 8hs usando la menor cantidad de maquinas.

# Declaracion de variables
# ------------------------
var SnL >= 0;
var SnR >= 0;

var ScL >= 0;
var ScR >= 0;

var MLSc >= 0;
var MRSc >= 0;
var MLSn >= 0;
var MRSn >= 0;

# Funcional:
minimize z: MLSc + MRSc + MLSn + MRSn;

# Restricciones:

s.t. cantSn: SnL + SnR = 10000;
s.t. cantSnEnML: SnL = 2 * 8 * MLSn;
s.t. cantSnEnMR: SnR = 5 * 8 * MRSn;

s.t. cantSc: ScL + ScR = 9000;
s.t. cantScEnML: ScL = 3 * 8 * MLSc;
s.t. cantScEnMR: ScR = 7 * 8 * MRSc;

s.t. dispML: MLSc + MLSn <= 60;
s.t. dispMR: MRSc + MRSn <= 70;

